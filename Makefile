project := zeus

.DEFAULT_GOAL : help

help:
	@echo "bootstrap - initialize local environement for development. Requires virtualenv."


check-virtual-env:
	@echo virtual-env: $${VIRTUAL_ENV?"Please run in virtual-env"}

bootstrap: check-virtual-env
	pip install -r requirements/dev.txt

clean: clean-build clean-pyc clean-test

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -rf {} +

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test:
	rm -f .coverage
	rm -f coverage.xml
	rm -fr htmlcov/

migration: ## Create migrations using alembic
	alembic revision --autogenerate -m "$(m)"

migrate-up: ## Run migrations using alembic
	alembic upgrade head

migrate-down: ## Rollback migrations using alembic
	alembic downgrade -1

pylint:
	pylint $(project)

pip-audit:
	pip-audit -r requirements/dev.txt -r requirements/common.txt

init-db:
	python ctrlsv_rest/initial_data.py

test:
	pytest -m "not integtest"

test-integtest:
	pytest -m "integtest" --reruns 5

coverage: clean-test
	coverage run --include=$(project)/* -m pytest -m "not integtest"
	coverage report -m
	coverage html
	open htmlcov/index.html

run:
	uvicorn $(project).main:app --reload
