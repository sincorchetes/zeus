| Technology name | Version | Type                       | Description                                                            |
| --------------- | ------- | -------------------------- | ---------------------------------------------------------------------- |
| Python          | 3.11    | Programming Language       | Universal purpose programming language                                 |
| Pytest          | 7.3.1   | Unit test                  | Library to perform tests                                               |
| Celery          | 5.2.7   | Queue manager              | Manage asynchronous                                                    |
| FastAPI         | 0.95.2  | Python API's framework     | Creates easily a FastAPI interface                                     |
| Uvicorn         | 0.22.0  | ASGI Web server for Python | Run FastAPI project                                                    |
| Coverage        | 7.2.7   | Check code                 | Perform code testing to get to know if the code is fully tested or not |
